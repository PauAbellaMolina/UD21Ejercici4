package convertidorDinero.convertidorDinero;


import dto.convertidorDineroFrame;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import methods.metodos;


/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
    
    public void testdolarEuro() {
    	convertidorDineroFrame calculadora = new convertidorDineroFrame();
        double esperado = 0;
        double delta = 50;
        assertEquals(metodos.dolarEuro(),esperado, delta);
    }
    public void testeuroYen() {
    	convertidorDineroFrame calculadora = new convertidorDineroFrame();
        double esperado = 0;
        double delta = 50;
        assertEquals(metodos.euroYen(),esperado, delta);
    }
    
    public void testyenEuro() {
    	convertidorDineroFrame calculadora = new convertidorDineroFrame();
        double esperado = 0;
        double delta = 50;
        assertEquals(metodos.yenEuro(),esperado, delta);
    }
    
    public void testdolarYen() {
    	convertidorDineroFrame calculadora = new convertidorDineroFrame();
        double esperado = 0;
        double delta = 50;
        assertEquals(metodos.dolarYen(),esperado, delta);
    }
    
    public void testyenDolar() {
    	convertidorDineroFrame calculadora = new convertidorDineroFrame();
        double esperado = 0;
        double delta = 50;
        assertEquals(metodos.yenDolar(),esperado, delta);
    }
    
    public void testeuroEuro() {
    	convertidorDineroFrame calculadora = new convertidorDineroFrame();
        double esperado = 0;
        double delta = 50;
        assertEquals(metodos.euroEuro(),esperado, delta);
    }
    
    public void testdolarDolar() {
    	convertidorDineroFrame calculadora = new convertidorDineroFrame();
        double esperado = 0;
        double delta = 50;
        assertEquals(metodos.dolarDolar(),esperado, delta);
    }
    
    public void testyenYen() {
    	convertidorDineroFrame calculadora = new convertidorDineroFrame();
        double esperado = 0;
        double delta = 50;
        assertEquals(metodos.yenYen(),esperado, delta);
    }
    

}
