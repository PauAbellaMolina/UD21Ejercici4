package methods;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import dto.convertidorDineroFrame;

public class metodos {
	
	//Este método es llamada por el main del programa, en el actionListener del botón Convertidor
	public static void botonConvertidor(JComboBox comboBoxArriba, JComboBox comboBoxAbajo, JTextField textFieldArriba, JTextField textFieldAbajo){
		if (comboBoxArriba.getSelectedItem().equals("Euro") && comboBoxAbajo.getSelectedItem().equals("Dolar")) {
			euroDolar();
			}
			if (comboBoxArriba.getSelectedItem().equals("Dolar") && comboBoxAbajo.getSelectedItem().equals("Euro")) {
				dolarEuro();
				}
			if (comboBoxArriba.getSelectedItem().equals("Euro") && comboBoxAbajo.getSelectedItem().equals("Yen")) {
				euroYen();
				}
			if (comboBoxArriba.getSelectedItem().equals("Yen") && comboBoxAbajo.getSelectedItem().equals("Euro")) {
				yenEuro();
				}
			if (comboBoxArriba.getSelectedItem().equals("Dolar") && comboBoxAbajo.getSelectedItem().equals("Yen")) {
				dolarYen();
				}
			if (comboBoxArriba.getSelectedItem().equals("Yen") && comboBoxAbajo.getSelectedItem().equals("Dolar")) {
				yenDolar();
				}
			if (comboBoxArriba.getSelectedItem().equals("Euro") && comboBoxAbajo.getSelectedItem().equals("Euro")) {
				euroEuro();
				}
			if (comboBoxArriba.getSelectedItem().equals("Dolar") && comboBoxAbajo.getSelectedItem().equals("Dolar")) {
				dolarDolar();				
				}
			if (comboBoxArriba.getSelectedItem().equals("Yen") && comboBoxAbajo.getSelectedItem().equals("Yen")) {
				yenYen();
				}
			
	}
	
	
	public static double euroDolar() {
		double resultParsedNumber = 0;
		try {
			//Intentamos hacer estas operaciones, en el caso de que den error, pasamo al catch (si lo que introducimos no es un número, por ejemplo)
			double parsedNumber = Double.parseDouble(convertidorDineroFrame.textFieldArriba.getText());
			resultParsedNumber = parsedNumber * 0.84;
			convertidorDineroFrame.textFieldAbajo.setText(String.valueOf(resultParsedNumber));
		} catch (Exception e) {
			//Una vez eclipse haya notificado el error, saldrá el mensaje a continuación. 
			JOptionPane.showMessageDialog(null, "ERROR: Introduce un número");
		}
		
		return resultParsedNumber;
		
	}
	
	public static double dolarEuro() {
		double resultParsedNumber = 0;
		try {
			//Intentamos hacer estas operaciones, en el caso de que den error, pasamo al catch (si lo que introducimos no es un número, por ejemplo)
			double parsedNumber = Double.parseDouble(convertidorDineroFrame.textFieldArriba.getText());
			resultParsedNumber = parsedNumber / 0.84;
			convertidorDineroFrame.textFieldAbajo.setText(String.valueOf(resultParsedNumber));
		} catch (Exception e) {
			//Una vez eclipse haya notificado el error, saldrá el mensaje a continuación. 
			JOptionPane.showMessageDialog(null, "ERROR: Introduce un número");
		}
		
		return resultParsedNumber;	
	}
	
	public static double euroYen() {
		double resultParsedNumber = 0;
		try {
			//Intentamos hacer estas operaciones, en el caso de que den error, pasamo al catch (si lo que introducimos no es un número, por ejemplo)
			double parsedNumber = Double.parseDouble(convertidorDineroFrame.textFieldArriba.getText());
			resultParsedNumber = parsedNumber * 125.70;
			convertidorDineroFrame.textFieldAbajo.setText(String.valueOf(resultParsedNumber));
		} catch (Exception e) {
			//Una vez eclipse haya notificado el error, saldrá el mensaje a continuación. 
			JOptionPane.showMessageDialog(null, "ERROR: Introduce un número");
		}
		return resultParsedNumber;	
	}
	
	public static double yenEuro() {
		double resultParsedNumber = 0;
		try {
			//Intentamos hacer estas operaciones, en el caso de que den error, pasamo al catch (si lo que introducimos no es un número, por ejemplo)
			double parsedNumber = Double.parseDouble(convertidorDineroFrame.textFieldArriba.getText());
			resultParsedNumber = parsedNumber / 125.70;
			convertidorDineroFrame.textFieldAbajo.setText(String.valueOf(resultParsedNumber));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "ERROR: Introduce un número");
		}
		return resultParsedNumber;	
	}
	
	public static double dolarYen() {
		double resultParsedNumber = 0;
		try {
			//Intentamos hacer estas operaciones, en el caso de que den error, pasamo al catch (si lo que introducimos no es un número, por ejemplo)
			double parsedNumber = Double.parseDouble(convertidorDineroFrame.textFieldArriba.getText());
			resultParsedNumber = parsedNumber * 105.50;
			convertidorDineroFrame.textFieldAbajo.setText(String.valueOf(resultParsedNumber));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "ERROR: Introduce un número");
		}
		return resultParsedNumber;	
	}
	
	public static double yenDolar() {
		double resultParsedNumber = 0;
		try {
			//Intentamos hacer estas operaciones, en el caso de que den error, pasamo al catch (si lo que introducimos no es un número, por ejemplo)
			double parsedNumber = Double.parseDouble(convertidorDineroFrame.textFieldArriba.getText());
			resultParsedNumber = parsedNumber / 105.50;
			convertidorDineroFrame.textFieldAbajo.setText(String.valueOf(resultParsedNumber));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "ERROR: Introduce un número");
		}
		return resultParsedNumber;	
	}
	
	public static double euroEuro() {
		double resultParsedNumber = 0;
		try {
			//Intentamos hacer estas operaciones, en el caso de que den error, pasamo al catch (si lo que introducimos no es un número, por ejemplo)
			double parsedNumber = Double.parseDouble(convertidorDineroFrame.textFieldArriba.getText());
			resultParsedNumber = parsedNumber;
			convertidorDineroFrame.textFieldAbajo.setText(String.valueOf(resultParsedNumber));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "ERROR: Introduce un número");
		}	
		return resultParsedNumber;	
	}
	
	public static double dolarDolar() {
		double resultParsedNumber = 0;
		try {
			//Intentamos hacer estas operaciones, en el caso de que den error, pasamo al catch (si lo que introducimos no es un número, por ejemplo)
			double parsedNumber = Double.parseDouble(convertidorDineroFrame.textFieldArriba.getText());
			resultParsedNumber = parsedNumber;
			convertidorDineroFrame.textFieldAbajo.setText(String.valueOf(resultParsedNumber));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "ERROR: Introduce un número");
		}	
		return resultParsedNumber;	
	}
	
	public static double yenYen() {
		double resultParsedNumber = 0;
		try {
			//Intentamos hacer estas operaciones, en el caso de que den error, pasamo al catch (si lo que introducimos no es un número, por ejemplo)
			double parsedNumber = Double.parseDouble(convertidorDineroFrame.textFieldArriba.getText());
			resultParsedNumber = parsedNumber;
			convertidorDineroFrame.textFieldAbajo.setText(String.valueOf(resultParsedNumber));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "ERROR: Introduce un número");
		}
		return resultParsedNumber;	
	}
}
