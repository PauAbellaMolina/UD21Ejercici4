package dto;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import methods.metodos;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class convertidorDineroFrame extends JFrame {

	private JPanel contentPane;
	public static JTextField textFieldArriba;
	public static JTextField textFieldAbajo;
	private JTextField tipoMonedaArriba;
	private JTextField tipoMonedaAbajo;
	private JComboBox comboBoxArriba;
	private JComboBox comboBoxAbajo;
	private static JButton btnConvertir;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					convertidorDineroFrame frame = new convertidorDineroFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public convertidorDineroFrame() {
		//Aquí asigno el método de cierre predeterminado
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//Aquí asigno las dimensiones
		setBounds(100, 100, 672, 406);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		//Aquí pongo el titulo de la ventana
		setTitle("Convertidor de Monedas");
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		//---------------------------------------- CREACIÓN DE BOTONES -------------------------------------------------
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldArriba.setText(""+textFieldArriba.getText().substring(0, textFieldArriba.getText().length() - 1));
			}
		});
		btnDelete.setBounds(539, 32, 89, 57);
		contentPane.add(btnDelete);
		
		JButton btnCE = new JButton("CE");
		btnCE.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldArriba.setText("");
				textFieldAbajo.setText("");
			}
		});
		btnCE.setBounds(447, 32, 89, 57);
		contentPane.add(btnCE);
		
		JButton btn9 = new JButton("9");
		btn9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldArriba.setText(textFieldArriba.getText()+"9");
			}
		});
		btn9.setBounds(539, 93, 89, 57);
		contentPane.add(btn9);
		
		JButton btn8 = new JButton("8");
		btn8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldArriba.setText(textFieldArriba.getText()+8);	
			}
		});
		btn8.setBounds(447, 93, 89, 57);
		contentPane.add(btn8);
		
		JButton btn7 = new JButton("7");
		btn7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldArriba.setText(textFieldArriba.getText()+"7");
			}
		});
		btn7.setBounds(355, 93, 89, 57);
		contentPane.add(btn7);
		
		JButton btn6 = new JButton("6");
		btn6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldArriba.setText(textFieldArriba.getText()+"6");
			}
		});
		btn6.setBounds(539, 154, 89, 57);
		contentPane.add(btn6);
		
		JButton btn5 = new JButton("5");
		btn5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldArriba.setText(textFieldArriba.getText()+"5");
			}
		});
		btn5.setBounds(447, 154, 89, 57);
		contentPane.add(btn5);
		
		JButton btn4 = new JButton("4");
		btn4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldArriba.setText(textFieldArriba.getText()+"4");
			}
		});
		btn4.setBounds(355, 154, 89, 57);
		contentPane.add(btn4);
		
		JButton btn3 = new JButton("3");
		btn3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldArriba.setText(textFieldArriba.getText()+"3");
			}
		});
		btn3.setBounds(539, 215, 89, 57);
		contentPane.add(btn3);
		
		JButton btn2 = new JButton("2");
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldArriba.setText(textFieldArriba.getText()+"2");
			}
		});
		btn2.setBounds(447, 215, 89, 57);
		contentPane.add(btn2);
		
		JButton btn1 = new JButton("1");
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldArriba.setText(textFieldArriba.getText()+"1");
			}
		});
		btn1.setBounds(355, 215, 89, 57);
		contentPane.add(btn1);
		
		JButton btnPunto = new JButton(".");
		btnPunto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldArriba.setText(textFieldArriba.getText()+".");
			}
		});
		btnPunto.setBounds(539, 276, 89, 57);
		contentPane.add(btnPunto);
		
		JButton btn0 = new JButton("0");
		btn0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldArriba.setText(textFieldArriba.getText()+"0");
			}
		});
		btn0.setBounds(447, 276, 89, 57);
		contentPane.add(btn0);
		
		//-----------------------------------------------------------------------------------------------------------------------------------
		//--------------------------------------------- AQUÍ SE VERÁN LOS NÚMEROS QUE HAS INTRODUCIDO/MOSTRADO ------------------------------
		textFieldArriba = new JTextField();
		textFieldArriba.setEditable(false);
		textFieldArriba.setBounds(10, 69, 115, 30);
		contentPane.add(textFieldArriba);
		textFieldArriba.setColumns(10);
		
		textFieldAbajo = new JTextField();
		textFieldAbajo.setEditable(false);
		textFieldAbajo.setColumns(10);
		textFieldAbajo.setBounds(10, 212, 115, 30);
		contentPane.add(textFieldAbajo);
		
		//-----------------------------------------------------------------------------------------------------------------------------------
		//--------------------------------------------- AQUÍ SE VERÁ EL SÍMBOLO DE LA MONEDA ------------------------------------------------
		
		tipoMonedaArriba = new JTextField();
		tipoMonedaArriba.setEditable(false);
		tipoMonedaArriba.setBounds(130, 69, 30, 30);
		contentPane.add(tipoMonedaArriba);
		tipoMonedaArriba.setColumns(10);
		
		tipoMonedaAbajo = new JTextField();
		tipoMonedaAbajo.setEditable(false);
		tipoMonedaAbajo.setColumns(10);
		tipoMonedaAbajo.setBounds(130, 212, 30, 30);
		contentPane.add(tipoMonedaAbajo);
		
		//------------------------------------------------------------------------------------------------------------------------------------
		//--------------------------------------------- AQUÍ MUESTRO DE QUE ELEMENTOS ESTÁN FORMADOS LOS COMBOBOX ----------------------------
		
		comboBoxArriba = new JComboBox();
		comboBoxArriba.setModel(new DefaultComboBoxModel(new String[] {"Euro", "Dolar", "Yen"}));
		comboBoxArriba.setBounds(10, 111, 115, 20);
		contentPane.add(comboBoxArriba);
		
		comboBoxAbajo = new JComboBox();
		comboBoxAbajo.setModel(new DefaultComboBoxModel(new String[] {"Dolar", "Euro", "Yen"}));
		comboBoxAbajo.setBounds(10, 252, 115, 20);
		contentPane.add(comboBoxAbajo);
		
		//-----------------------------------------------------------------------------------------------------------------------------------
		//---- AQUÍ CREO UN BOTÓN DONDE CONVIERTES EL VALOR DEL TEXTFIELD DE ARRIBA AL DE ABAJO DEPEINDIENDO DEL TIPO DE MONEDA -------------
		
		btnConvertir = new JButton("Convertir");
		btnConvertir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//En este método modifico el valor del primer JTextField al segundo JTextField dependiendo del tipo de moneda
				metodos.botonConvertidor(comboBoxArriba,comboBoxAbajo,textFieldArriba,textFieldAbajo);
			}
		});
		btnConvertir.setBounds(174, 137, 126, 57);
		contentPane.add(btnConvertir);
		//-----------------------------------------------------------------------------------------------------------------------------------
	}
}
